module.exports = (req,res,next)=>{
    const {name} = req.body;
    req.body.name = name.replace(/\s{2,}/g, ' ').trim() 
    next();
}