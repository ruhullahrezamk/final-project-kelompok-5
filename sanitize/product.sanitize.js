module.exports = (req,res,next)=>{
    const {nama,deskripsi} = req.body;
    req.body.nama = nama.replace(/\s{2,}/g, ' ').trim() 
    req.body.deskripsi = deskripsi.replace(/\s{2,}/g, ' ').trim() 
    next();
}