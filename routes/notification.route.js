const router = require("express").Router();
const {getSellerNotification,getProductNotification,getBuyerNotification} = require('../controllers/notification.controller')

router.get("/seller", getSellerNotification);
router.get("/product", getProductNotification);
router.get("/buyer", getBuyerNotification);

module.exports = router;