const router = require("express").Router();
const mainRoutes = require("./main.route.js");
const profileRoutes = require("./profile.route");
const productRoutes = require("./product.route");
const penawaranRoutes = require("./penawaran.route");
const notificationRoutes = require("./notification.route");
const dataRoutes = require("./data.route");

const authHeader = require("../misc/auth.header");

router.use("/", mainRoutes);
router.use("/profile", authHeader, profileRoutes);
router.use("/product", productRoutes);
router.use("/data", dataRoutes);
router.use("/transaction", authHeader, penawaranRoutes);
router.use("/notification", authHeader, notificationRoutes);

module.exports = router;
