const router = require('express').Router();
const { regist, login} = require('../controllers/main.controller');
const registerValidation = require('../validation/register.validation')
const loginValidation = require('../validation/login.validation')

const registerSanitize = require('../sanitize/register.sanitize')

router.post('/register', registerValidation, registerSanitize,regist);
router.post('/login', loginValidation,login);

module.exports = router;