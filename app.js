require("dotenv").config();
const express = require('express');
const app = express();
const routes = require('./routes');
const morgan = require('morgan');

const errorHandler = require('./misc/error')

const cors = require('cors')

app.use(cors({origin: '*'})); 

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(morgan('dev'));
app.use('/api',routes);
app.use(errorHandler);

app.get('/',(req,res)=>{
    res.status(200).json({
        messasge:'welcome to second hand kelompok 5'
    })
})

module.exports = app;