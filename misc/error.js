const Response = require('../helpers/response')
module.exports = (err, req, res, next) => {
    const response = new Response(res)
    
    const limitUpload = err.code === "LIMIT_UNEXPECTED_FILE"  
    if(limitUpload) return response.Fail(response.BadRequest,'Bad Request','maksimal upload 4 file')

    const invalidJson = err.type === "entity.parse.failed"
    if(invalidJson) return response.Fail(response.BadRequest,'Bad Request','invalid JSON')

    if(err.code) return response.Fail(err.code,err.status,err.message)
    
    if(err === 'invalid file extension') return response.Fail(response.BadRequest,'Bad Request','valid file extension are .png, .jpeg, .jpg')

    if(err) return response.Fail(response.InternalServerError,'internal server error')  
}