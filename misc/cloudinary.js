const cloudinary = require('cloudinary').v2;
const Response = require('../helpers/response')
require('dotenv').config()

cloudinary.config({ 
    cloud_name: process.env.cloud_name, 
    api_key: +process.env.api_key, 
    api_secret: process.env.api_secret,
    secure: true
});

const profileUpload = async (req, res, next) => {
    const response = new Response(res)
    try {
        if(req.file){
            const foldering = `secondHand/profile/`;
            const uploadedMedia = await cloudinary.uploader.upload(req.file.path, {
                folder: foldering
            });
            req.body.cloud_media_url = uploadedMedia.secure_url;
        }   
        next();
    } catch (error) {
        return response.Fail(error.http_code,error,error.message)
    }
};

const productUpload = async (file,id) => {
    try {
        const foldering = `secondHand/product/`;
        const uploadedMedia = await cloudinary.uploader.upload(file.path, {
            public_id:`${id}-${file.name}`,
            folder: foldering
        });

        const cloud_media_url = uploadedMedia.secure_url;
        return cloud_media_url

    } catch (error) {
        throw(error)
    }
};

const deleteImage = async (img_url) => {
    const img_name = img_url.split('/').pop()
    const img_folder = img_url.split('/')[8]
    const img_id = img_name.split('.')[0]
    return await cloudinary.uploader.destroy(`secondHand/${img_folder}/${img_id}`, result => result)
}

module.exports = {profileUpload,productUpload,deleteImage};