'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'products', // table name
      'published', // new field name
      {
        type: Sequelize.BOOLEAN,
        defaultValue:false
      }
    )
    await queryInterface.removeColumn('products', 'img_url')
  },

  async down (queryInterface, Sequelize) {
   
    queryInterface.removeColumn('products', 'published')
  }
};
