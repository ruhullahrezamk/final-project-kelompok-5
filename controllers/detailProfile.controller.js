const { profile, account, kota } = require("../models");
const Response = require("../helpers/response");

const { deleteImage } = require("../misc/cloudinary");

const editProfile = async (req, res, next) => {
  const response = new Response(res);
  const { name, address, phone_number, city_id, cloud_media_url } = req.body;
  const { id } = req.user;
  try {

    const foundAccount = await account.findOne({ where: { id: req.user.id } });
    if (!foundAccount) throw { message: "Account not found", status: "Not Found", code: 404 };

    const foundKota = await kota.findOne({where:{id:city_id}})
    if(!foundKota) throw { message: "city_id not found", status: "Not Found", code: 404 };

    const currentProfile = await profile.findOne({ where: { account_id: id } });
    if (!currentProfile)
      await profile.create({
        name,
        address,
        phone_number,
        city_id,
        img_url: cloud_media_url,
      });

    const updatedData = { name, address, city_id, phone_number };
    if (cloud_media_url) {
      const currentImageUrl = currentProfile.img_url;
      if (currentImageUrl) deleteImage(currentImageUrl);
      updatedData.img_url = cloud_media_url;
    }

    const detailProfilEdit = await profile.update(updatedData, {
      where: { account_id: id },
    });
    if (detailProfilEdit)
      return response.Success(response.Ok, "profile berhasil diupdate");
  } catch (error) {
    next(error);
  }
};

const getProfile = async (req, res, next) => {
  const response = new Response(res);
  const { id } = req.user;
  try {
    const foundAccount = await account.findOne({ where: { id } });
    if (!foundAccount)
      throw { message: "Account not found", status: "Not Found", code: 404 };

    const currentProfile = await profile.findOne({
      raw: true,
      include: [
        {
          model: kota,
          attributes: ["nama_kota"],
        },
      ],
      attributes: [
        "id",
        "name",
        "phone_number",
        "address",
        "city_id",
        "img_url",
      ],
      where: {
        account_id: id,
      },
    });

    if (currentProfile) {
      currentProfile.kota = currentProfile["kotum.nama_kota"];
      delete currentProfile["kotum.nama_kota"];
      return response.Success(response.Ok, currentProfile);
    }
  } catch (error) {
    next(error);
  }
};

module.exports = { getProfile, editProfile };
