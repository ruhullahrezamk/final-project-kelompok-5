const { account, profile } = require("../models");
const { signToken, checkPassword } = require("../misc/auth");

const regist = async (req, res, next) => {
  const { email, password, name } = req.body;
  try {
    const foundUser = await account.findOne({ where: { email } });
    if (foundUser)
      throw {
        message: "email address is already being used",
        status: "Bad Request",
        code: 400,
      };

    const registUser = await account.create({
      email: email,
      password: password,
    });

    if (registUser) {
      await profile.create({
        name: name,
        account_id: registUser.id,
      });
    }

    res.status(201).json({
      status: "Created",
      data: "Successfully create account",
    });
  } catch (error) {
    next(error);
  }
};

//login
const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const foundUser = await account.findOne({
      include:[{
        model:profile,
        attributes:["name"]
      }],
      where: {
        email: email,
      },
    });

    if (!foundUser)
      throw { message: "email is incorrect", status: "Bad Request", code: 400 };

    const passwordTrue = checkPassword(password, foundUser.password);

    if (passwordTrue) {
      const payload = {
        id: foundUser.id,
        name: foundUser.profile.name,
      };

      const token = signToken(payload);
      return res.status(200).json({
        data: token,
      });
    }
    return res.status(400).json({
      status: "Bad Request",
      message: "password is incorrect",
    });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  regist,
  login
};
