const Response = require('../helpers/response')
module.exports = (req,res,next)=>{
    const response = new Response(res);
    const {harga} = req.body;

    if(!harga) return response.Fail(response.BadRequest,'Bad Request','harga tidak boleh kosong')
 
    if(harga < 0) return response.Fail(response.BadRequest,'Bad Request','harga harus bernilai positif')
      
    next();
}