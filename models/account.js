"use strict";

const bcrypt = require("bcryptjs");
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class account extends Model {
    static associate(models) {
      account.hasOne(models.profile, {
        foreignKey: "account_id",
      });
      account.hasMany(models.product, {
        foreignKey: "account_id",
      });
      account.hasMany(models.penawaran, {
        foreignKey: "id_pembeli",
      });
    }
  }
  account.init(
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "account",
      hooks: {
        beforeCreate: (account, options) => {
          account.password = bcrypt.hashSync(
            account.password,
            +process.env.SALT_ROUNDS
          );
          return account;
        },
      },
    }
  );
  return account;
};
