"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    static associate(models) {
      product.belongsTo(models.account, {
        foreignKey: "account_id",
      });
      product.belongsTo(models.category, {
        foreignKey: "kategori_id",
      });
      product.hasMany(models.picture, {
        foreignKey: "product_id",
      });
      product.hasMany(models.penawaran, {
        foreignKey: "id_product",
      });
    }
  }

  product.init(
    {
      nama: DataTypes.STRING,
      harga: DataTypes.INTEGER,
      kategori_id: DataTypes.INTEGER,
      deskripsi: DataTypes.STRING,
      account_id: DataTypes.INTEGER,
      is_sold: DataTypes.BOOLEAN,
      published: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "product",
    }
  );
  
  return product;
};
