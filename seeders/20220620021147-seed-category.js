'use strict';
const categoryData = require('../masterData/category.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const category = categoryData.map((eachcategory) => {
      eachcategory.createdAt = new Date();
      eachcategory.updatedAt = new Date();
      return eachcategory;
    });

   await queryInterface.bulkInsert('categories', category);
  },

  async down (queryInterface, Sequelize) {

     await queryInterface.bulkDelete('categories', null, { truncate: true, restartIdentity: true }) 
  }
};
