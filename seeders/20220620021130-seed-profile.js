'use strict';
const profileData = require('../masterData/profile.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const profile = profileData.map((eachprofile) => {
      eachprofile.createdAt = new Date();
      eachprofile.updatedAt = new Date();
      return eachprofile;
    });

   await queryInterface.bulkInsert('profiles', profile);
  },

  async down (queryInterface, Sequelize) {

     await queryInterface.bulkDelete('profiles', null, { truncate: true, restartIdentity: true }) 
  }
};
