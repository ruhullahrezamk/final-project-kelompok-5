'use strict';
const accountData = require('../masterData/account.json')
const bcrypt = require('bcrypt')
require('dotenv').config()

module.exports = {
  async up (queryInterface, Sequelize) {
    const salt = await bcrypt.genSalt(+process.env.salt_round)
    const account = accountData.map((eachAccount) => {
      eachAccount.password  = bcrypt.hashSync(eachAccount.password,salt)
      eachAccount.createdAt = new Date();
      eachAccount.updatedAt = new Date();
      return eachAccount;
    });

   await queryInterface.bulkInsert('accounts', account);
  },

  async down (queryInterface, Sequelize) {

     await queryInterface.bulkDelete('accounts', null, { truncate: true, restartIdentity: true }) 
  }
};
