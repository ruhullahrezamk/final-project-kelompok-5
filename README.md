# final-project-kelompok-5

Repository ini berisi dari source code API SecondHand.

SecondHand Merupakan website e-commerce tempat berjual beli barang bekas. website ini dibuat sebagai final project dari program Backend Javascript di Binar Academy.

API SecondHand berisi beberapa endpoint yang dibagi dalam beberapa kelompok

User berisi endpoint    : Register, Login, Update Profile, Get Profile

Data berisi endpoint    : Get data kota, Get data kategori

Product berisi endpoint : CRUD Product

Transaction berisi endpoint : CRUD Transaction

Notification berisi endpoint: Get Notifiaction Seller, Get Notifiaction Product, Get Notifiaction Buyer 

## Dokumentasi API SECONDHAND
https://documenter.getpostman.com/view/17275912/UzBnq6F8

